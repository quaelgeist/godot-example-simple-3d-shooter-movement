extends KinematicBody

const MAX_SPEED = 10
const ROTATE_SPEED = 1
const ROTATE_MULT = 1


var camera
var mouse_pos
var new_mouse_pos


var movement = Vector3()
var rotation = Vector3()


func move_object(delta):
	var new_transform = get_transform()
	var new_cam_transform = camera.get_transform()
	
	new_transform = new_transform.translated(movement)
	
	var phi = 0.002
	
	new_transform = new_transform.rotated(Vector3(0,rotation.y,0), phi)
	new_cam_transform = new_cam_transform.rotated(Vector3(rotation.x,0,0), phi)
	
	set_transform(new_transform)
	camera.set_transform(new_cam_transform)


func mouse_controls():
	Input.warp_mouse_pos(Vector2(0,0))
	
	new_mouse_pos = Input.get_mouse_pos()
	
	#I think there is a bug in the engine.
	#the warp_mouse_pos does not really warp the mouse
	#reliable. there is always a miss of -1/-1 (Vector)
	#we will prevent it with just add 1 to this malfuncionality.
	
#	new_mouse_pos.x += 1
#	new_mouse_pos.y += 1
		
	if new_mouse_pos != mouse_pos:
		mouse_pos = new_mouse_pos
		
		rotation.x = mouse_pos.y
		rotation.y = mouse_pos.x


func controls(delta):
	
	#movement
	var act_speed = MAX_SPEED * delta
	
	if Input.is_action_pressed("left"):
		movement.x = -act_speed
	elif Input.is_action_pressed("right"):
		movement.x = act_speed
	else:
		movement.x = 0
	
	if Input.is_action_pressed("forward"):
		movement.z = -act_speed
	elif Input.is_action_pressed("backward"):
		movement.z = act_speed
	else:
		movement.z = 0
	
	if Input.is_action_pressed("up"):
		movement.y = act_speed
	elif Input.is_action_pressed("down"):
		movement.y = -act_speed
	else:
		movement.y = 0
	
	#keyboard rotation
	var act_rot_speed = ROTATE_SPEED * ROTATE_MULT
	
	if Input.is_action_pressed("look_up"):
		rotation.x = -act_rot_speed
	elif Input.is_action_pressed("look_down"):
		rotation.x = act_rot_speed
	else:
		rotation.x = 0

	if Input.is_action_pressed("look_left"):
		rotation.y = -act_rot_speed
	elif Input.is_action_pressed("look_right"):
		rotation.y = act_rot_speed
	else:
		rotation.y = 0
	rotation.z = 0
	
	#mouse rotation
	mouse_controls()


func _process(delta):
	
	controls(delta)
	move_object(delta)
	
	print("speed ? : " , get_transform())
	

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	mouse_pos = Input.get_mouse_pos()
	
	camera = self.get_node("Camera")
	set_process(true)

